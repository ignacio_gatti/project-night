﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Hello users, would you please enter your name? Name: ");
            string user = Console.ReadLine();
            Console.Clear();
            Console.WriteLine($"Nice to meet you {user}, I'm your instructor");
            Console.WriteLine("I've got a project in mind for you to do.");
            Console.WriteLine("I love MadLib documents, and that's what I'd like you to make.");
            Console.ReadKey();
                        
            Console.Clear();
            Console.WriteLine("Here's an example for you");
            Console.WriteLine("I've been on so many blind dates this year, I actually went to dinner with the same {INSERT NOUN} twice!");
            Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine("Now I'm told to put a NOUN in the appropriate space");
            Console.WriteLine("For example: bird");
            Console.ReadKey();
            Console.WriteLine();
            Console.WriteLine("Our new sentence would be");
            Console.WriteLine("I've been on so many blind dates this year, I actually went to dinner with the same bird twice!");
            Console.WriteLine();
            Console.WriteLine("What a LOL-ercoaster");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
           

            Console.Clear();
            Console.WriteLine("Ok, so here's what I want. I'd like for you to make a program where a user is shown a sentence/paragraph");
            Console.WriteLine("5 of the words should spaces where the user will know that their input will be recieved");
            Console.WriteLine("The user should be able to input 5 words");
            Console.WriteLine("These inputs will then be used to modify the sentence");
            Console.WriteLine("The sentence, with the users modified words, will then be output to the Console windows");
            Console.WriteLine();
            Console.WriteLine("Good luck");
            var agile = Console.ReadKey();

            if (agile.Key == ConsoleKey.F12)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.Clear();
                Console.WriteLine("Welcome to the next level of this project");
                Console.WriteLine("I was thinking about what I asked you, and it's just not doing it");
                Console.WriteLine();
                Console.WriteLine("What I was thinking is the mad lib sentence/paragraph that you've made should be random each time it's run");
                Console.WriteLine("So what I'd like you to do is store the a list/array of 50 words");
                Console.WriteLine("Then, put random words into your sentence/paragraph");
                Console.WriteLine();
                Console.WriteLine("You'll know that you've done it correctly when each time you create a new sentence the words are different (most of the time)");
                Console.WriteLine("Good Luck");
                agile = Console.ReadKey();

                if (agile.Key == ConsoleKey.C)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Clear();
                    Console.WriteLine("One final thing, I was thinking this project is still missing something");
                    Console.WriteLine("Why make a user open the program to see the information, I think it's a little much");
                    Console.WriteLine();
                    Console.WriteLine("All that I ask is to make a loop so that the console window doesn't close until the user wants it to");
                    Console.WriteLine("One more thing, the user should be able to choose how many times it wants the loop to run");
                    Console.WriteLine("So that they don't get over burdened with MadLibs");
                    Console.WriteLine();
                    Console.WriteLine("Good Luck");
                    Console.ReadKey();
                }
            }
        }
    }
}
